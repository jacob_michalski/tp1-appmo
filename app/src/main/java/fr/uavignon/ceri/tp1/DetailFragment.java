package fr.uavignon.ceri.tp1;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import fr.uavignon.ceri.tp1.data.Country;

public class DetailFragment extends Fragment {

    TextView nom;
    ImageView flag;
    TextView city;
    TextView language;
    TextView currency;
    TextView population;
    TextView surface;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nom = view.findViewById(R.id.textView);
        flag = view.findViewById(R.id.imageView);
        city = view.findViewById(R.id.editTextTextPersonName);
        language = view.findViewById(R.id.editTextTextPersonName2);
        currency = view.findViewById(R.id.editTextTextPersonName3);
        population = view.findViewById(R.id.editTextTextPersonName4);
        surface = view.findViewById(R.id.editTextTextPersonName5);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        nom.setText(Country.countries[args.getCountryId()].getName());
        flag.setImageResource(Country.countries[args.getCountryId()].getDrawable());
        city.setText(Country.countries[args.getCountryId()].getCapital());
        language.setText(Country.countries[args.getCountryId()].getLanguage());
        currency.setText(Country.countries[args.getCountryId()].getCurrency());
        population.setText(String.valueOf(Country.countries[args.getCountryId()].getPopulation()));
        surface.setText(String.valueOf(Country.countries[args.getCountryId()].getArea()));

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections action =
                        DetailFragmentDirections.actionSecondFragmentToFirstFragment();
                Navigation.findNavController(v).navigate(action);
            }
        });
    }
}