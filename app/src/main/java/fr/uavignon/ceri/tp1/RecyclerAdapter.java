package fr.uavignon.ceri.tp1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import fr.uavignon.ceri.tp1.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{

    static class ViewHolder extends RecyclerView.ViewHolder {
        static ImageView itemImage;
        static TextView itemPays;
        static TextView itemCapitale;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemPays = itemView.findViewById(R.id.item_pays);
            itemCapitale = itemView.findViewById(R.id.item_capitale);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action =
                            ListFragmentDirections.actionFirstFragmentToSecondFragment(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        ViewHolder.itemPays.setText(Country.countries[i].getName());
        ViewHolder.itemCapitale.setText(Country.countries[i].getCapital());
        ViewHolder.itemImage.setImageResource(Country.countries[i].getDrawable());

    }

    @Override
    public int getItemCount() {
        return Country.countries.length;
    }
}
